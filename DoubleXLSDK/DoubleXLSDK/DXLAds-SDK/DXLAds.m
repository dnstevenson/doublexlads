//
//  DXLAds.m
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import "DXLAds.h"
#import "DXLHTTPClient.h"
#import "DXLAd.h"
#import "UIImageView+DXL_AFNetworking.h"

static NSString *apiKey;

@implementation DXLAds


- (void) adShown:(DXLAd*) ad {
    UIImageView *tempView = [[UIImageView alloc] init];
    [tempView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:ad.veripixel]] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        //
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        //
    }];
}


- (void) getAd:(void (^)(DXLAd *ad)) success failure:(void (^)(NSString *errorMessage)) failure {
    
    //[{"networkId":4216, "siteId":20036, "divName":"1", "adTypes":[755], "flightId":44182}]
    
    NSString *data = [NSString stringWithFormat:@"[{\"networkId\":4216, \"siteId\":%i, \"divName\":\"1\", \"adTypes\":[23]}]", [apiKey intValue]];
    //NSLog(@"request data: %@", data);
    NSString *encodedRequest = [data stringByAddingPercentEscapesUsingEncoding:
                                NSASCIIStringEncoding];
    
	NSString *urlString = [NSString stringWithFormat:@"http://engine.adzerk.net/api/v1?request=%@",
						   encodedRequest];
    
    NSLog(@"url: %@", urlString);
    
   [[DXLHTTPClient sharedInstance] getAd:urlString success:^(NSDictionary *json) {
       DXLAd *ad = [[DXLAd alloc] init];
       ad.clickURL = json[@"clickURL"];
       ad.imageURL = json[@"imageURL"];
       ad.veripixel = json[@"veripixel"];
       success(ad);
   } failure:^(NSString *error) {
       NSLog(@"getAd failure: %@", error);
       failure(error);
   }];

}

+ (void) startSessionWithAPIKey:(NSString *)inAPIKey {
	apiKey = inAPIKey;
}

@end
