//
//  DXLAdCardViewController.h
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 3/2/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DXLAdCardViewController : UIViewController

@property (nonatomic) NSString *urlString;

@end
