//
//  DXLAdCardViewController.m
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 3/2/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import "DXLAdCardViewController.h"

@interface DXLAdCardViewController ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)close:(id)sender;

@end

@implementation DXLAdCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}
@end
