//
//  DXLHTTPClient.h
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DXL_AFNetworking.h"

@interface DXLHTTPClient : DXL_AFHTTPClient

- (void) getAd:(NSString*) urlString success:(void (^)(NSDictionary *json)) success failure:(void (^)(NSString *error)) failure;

+ (DXLHTTPClient *) sharedInstance;

@end
