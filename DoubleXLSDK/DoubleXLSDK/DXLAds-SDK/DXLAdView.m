//
//  DXLAdView.m
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import "DXLAdView.h"
#import "DXLAd.h"
#import "DXLAds.h"
#import "DXLAdCardViewController.h"
#import "UIImageView+DXL_AFNetworking.h"
#import "DXL_SVProgressHUD.h"

#define REFRESH_RATE 60

@interface UIView (FindUIViewController)

- (UIViewController*)firstAvailableUIViewController;

- (id)traverseResponderChainForUIViewController;

@end

@implementation UIView (FindUIViewController)

- (UIViewController*)firstAvailableUIViewController {
    return (UIViewController *)[self traverseResponderChainForUIViewController];
}

- (id)traverseResponderChainForUIViewController {
    id nextResponder = [self nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]]) {
        return nextResponder;
    } else if ([nextResponder isKindOfClass:[UIView class]]) {
        return [nextResponder traverseResponderChainForUIViewController];
    } else {
        return nil;
    }
}

@end


@interface DXLAdView()

@property (nonatomic) DXLAds *adServer;
@property (nonatomic) DXLAd *currentAd;
@property (nonatomic) NSURL *redirectedClickURL;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) BOOL didResizeParentView;


@end

@implementation DXLAdView

- (void) fadeAdIn {
	[self.adServer getAd:^(DXLAd *ad) {
        [self.adServer adShown:ad];
        [self didGetNextAd:ad];
    } failure:^(NSString *errorMessage) {
        [self didNotGetAd];
    }];
}

- (void) resizeParentViewForAdView {
    if (self.animate) {
        self.alpha = 1.0;
        self.backgroundColor = [UIColor clearColor];
        if (!self.didResizeParentView) {
            CGPoint bannerOrigin = self.frame.origin;
            CGFloat bannerHeight = self.frame.size.height;
            bannerOrigin.y -= bannerHeight;
            
            self.frame = CGRectMake(bannerOrigin.x, bannerOrigin.y, self.frame.size.width, self.frame.size.height);
            
            if (self.parentViewToResize) {
                CGRect contentFrame = self.parentViewToResize.frame;
                contentFrame.size.height -= bannerHeight;
                self.parentViewToResize.frame = contentFrame;
                [self.parentViewToResize layoutIfNeeded];
            }
            self.didResizeParentView = YES;
        }
    }
}
- (void) hideAdViewFromParent {
    if (self.animate) {
        if (self.didResizeParentView) {
            
            CGRect contentFrame;
            CGPoint bannerOrigin = self.frame.origin;
            CGFloat bannerHeight = self.frame.size.height;

            if (self.parentViewToResize) {
                contentFrame = self.parentViewToResize.frame;
                bannerOrigin = CGPointMake(CGRectGetMinX(contentFrame), CGRectGetMaxY(contentFrame));
                contentFrame.size.height += bannerHeight;
                self.parentViewToResize.frame = contentFrame;
                [self.parentViewToResize layoutIfNeeded];
            }
            else {
                bannerOrigin.y += bannerHeight;            
            }

            self.frame = CGRectMake(bannerOrigin.x, bannerOrigin.y, self.frame.size.width, self.frame.size.height);
            self.didResizeParentView = NO;
        }
        else {
            CGPoint bannerOrigin = self.frame.origin;
            CGFloat bannerHeight = self.frame.size.height;
            bannerOrigin.y += bannerHeight;
            self.frame = CGRectMake(bannerOrigin.x, bannerOrigin.y, self.frame.size.width, self.frame.size.height);
        }
    }
}
- (void) didNotGetAd {
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:REFRESH_RATE target:self selector:@selector(changeAd) userInfo:nil repeats:NO];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReceiveAd)]) {
        [self.delegate didNotReceiveAd];
    }
        
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 0;
        [self hideAdViewFromParent];
    } completion:^(BOOL finished) {
        //
    }];

}


- (void) didGetNextAd:(DXLAd *)ad {
    
    self.currentAd = ad;
    [self setImageWithURL:[NSURL URLWithString:ad.imageURL]];

    [UIView animateWithDuration:0.4 animations:^{
        [self resizeParentViewForAdView];
    } completion:^(BOOL finished) {
        
    }];
    
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:REFRESH_RATE target:self selector:@selector(changeAd) userInfo:nil repeats:NO];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didReceiveAd)]) {
        [self.delegate didReceiveAd];
    }
    
}

- (void) changeAd {
    
    [UIView animateWithDuration:0.5 animations:^{
        [self fadeAdIn];
    } completion:^(BOOL finished) {
        
    }];
    
}

- (void) startAds {
    [self hideAdViewFromParent];
    [self changeAd];
}

- (void) stopAds {
    [self.timer invalidate];
}

// *** When the ad view is touched, a web view is shown using the current target URL
- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	[DXL_SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.currentAd.clickURL]] delegate:self startImmediately:YES];
    conn = nil;
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response
{
    self.redirectedClickURL = [response URL];
    
    if( [self.redirectedClickURL.host hasSuffix:@"itunes.apple.com"])
    {
		[DXL_SVProgressHUD dismiss];
        [connection cancel];
        NSLog(@"redirected: %@", self.redirectedClickURL.absoluteString);
        [[UIApplication sharedApplication] openURL:self.redirectedClickURL];
        return nil;
    }
    else
    {
        return request;
    }
}

// No more redirects; use the last URL saved
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[DXL_SVProgressHUD dismiss];

	DXLAdCardViewController *controller = [[DXLAdCardViewController alloc] initWithNibName:@"DXLAdCardViewController" bundle:nil];
    controller.urlString = self.redirectedClickURL.absoluteString;
	controller.modalPresentationStyle = UIModalPresentationFullScreen;

	UIViewController *parentViewController = [self firstAvailableUIViewController];
	[parentViewController presentViewController:controller animated:YES completion:nil];
}


- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.adServer = [[DXLAds alloc] init];
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        self.animate = YES;
    }
    
    return self;
}
- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.adServer = [[DXLAds alloc] init];
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        self.animate = YES;
    }
    return self;
}

@end
