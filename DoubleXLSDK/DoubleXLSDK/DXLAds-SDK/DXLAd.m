//
//  DXLAd.m
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import "DXLAd.h"

@interface DXLAd()

@end


static NSString *apiKey;

@implementation DXLAd

+ (void) startSessionWithAPIKey:(NSString *)inAPIKey {
	apiKey = inAPIKey;
}


@end
