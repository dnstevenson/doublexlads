//
//  DXLAdView.h
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol DXLAdViewDelegate <NSObject>
@optional
-(void) didReceiveAd;
-(void) didNotReceiveAd;
@end

@interface DXLAdView : UIImageView

@property (nonatomic, weak) id <DXLAdViewDelegate> delegate;
@property (nonatomic) NSString *clickURL;
@property (nonatomic) UIView *parentViewToResize;
@property (nonatomic) BOOL animate;

- (void) startAds;
- (void) stopAds;
@end
