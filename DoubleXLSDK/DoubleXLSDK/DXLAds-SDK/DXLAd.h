//
//  DXLAd.h
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DXLAd : NSObject


@property (nonatomic) UIImage *image;
@property (nonatomic) NSString *imageURL;
@property (nonatomic) NSString *clickURL;
@property (nonatomic) NSString *veripixel;

@end
