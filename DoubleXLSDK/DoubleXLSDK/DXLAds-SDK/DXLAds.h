//
//  DXLAds.h
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DXLAd.h"

@interface DXLAds : NSObject

+ (void) startSessionWithAPIKey:(NSString *)applicationKey;

- (void) getAd:(void (^)(DXLAd *ad)) success failure:(void (^)(NSString *errorMessage)) failure;
- (void) adShown:(DXLAd*) ad;


@end
