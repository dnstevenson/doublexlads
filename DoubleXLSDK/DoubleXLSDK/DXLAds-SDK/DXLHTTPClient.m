//
//  DXLHTTPClient.m
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import "DXLHTTPClient.h"
#import "UIImageView+DXL_AFNetworking.h"

@interface DXLHTTPClient()

@property (nonatomic) BOOL hasNetwork;

@end

@implementation DXLHTTPClient

- (void) getAd:(NSString*) urlString success:(void (^)(NSDictionary *json)) success failure:(void (^)(NSString *error)) failure {
    if (self.hasNetwork) {
        NSURLRequest *request = [[DXLHTTPClient sharedInstance] requestWithMethod:@"GET" path:urlString parameters:nil];
        DXL_AFJSONRequestOperation* operation = [DXL_AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            //NSLog(@"%@", JSON);
            
            NSDictionary *first = JSON[@"1"];
            if (![first isKindOfClass:NSNull.class] && [[first allKeys] count] > 0) {
                NSString *clickURL = first[@"click"];
                NSString *imageURL = first[@"image"];
                NSString *veripixel = first[@"veripixel"];
                
                NSMutableDictionary *returnParams = [NSMutableDictionary dictionary];
                returnParams[@"clickURL"] = clickURL;
                returnParams[@"imageURL"] = imageURL;
                returnParams[@"veripixel"] = veripixel;
                success(returnParams);
            }
            else {
                NSLog(@"no ad: %@", JSON);
                failure(@"no_ad");
            }
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            NSLog(@"fail: %@", [error description]);
            failure(@"fail");
        }];
        [operation start];
    }
    else {
        failure(@"no_ad");
    }
    
}
+ (DXLHTTPClient *) sharedInstance {
    static DXLHTTPClient *instance = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^ {
        instance = [[self alloc] init]; //[[self alloc] initWithBaseURL:[NSURL URLWithString:WU_SERVER_URL]];
       
        instance.hasNetwork = YES;
        
        [instance setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            NSLog(@"reachability: %i", status);
            if (status > 0) {
                instance.hasNetwork = YES;
            }
            else {
                instance.hasNetwork = NO;
            }
        }];
    });
    
    return instance;
    
}

@end
