//
//  DXLViewController.m
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import "DXLViewController.h"
#import "DXLAdView.h"

@interface DXLViewController ()
@property (strong, nonatomic) IBOutlet DXLAdView *adView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation DXLViewController

-(void) didNotReceiveAd {
    //NSLog(@"did not get ad");
}
- (void) didReceiveAd {
    //NSLog(@"got ad");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat:@"Row %i", indexPath.row];
    
    return cell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.adView.delegate = self;
    self.adView.parentViewToResize = self.tableView;
    [self.adView startAds];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
