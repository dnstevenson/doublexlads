//
//  DXLViewController.h
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 2/21/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXLAdView.h"

@interface DXLViewController : UIViewController <DXLAdViewDelegate>

@end
