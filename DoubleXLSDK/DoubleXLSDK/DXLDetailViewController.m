//
//  DXLDetailViewController.m
//  DoubleXLSDK
//
//  Created by Dave Stevenson on 3/8/13.
//  Copyright (c) 2013 Dave Stevenson. All rights reserved.
//

#import "DXLDetailViewController.h"
#import "DXLAdView.h"

@interface DXLDetailViewController ()

@property (strong, nonatomic) IBOutlet DXLAdView *adView;

@end

@implementation DXLDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.adView.delegate = self;
    //self.adView.parentViewToResize = self.view;
    [self.adView startAds];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
